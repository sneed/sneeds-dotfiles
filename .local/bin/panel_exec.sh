#!/bin/sh

getcolors () {
	FOREGROUND=$(xrdb -query | grep 'foreground:'| awk '{print $NF}')
	BACKGROUND=$(xrdb -query | grep 'background:'| awk '{print $NF}')
	BLACK=$(xrdb -query | grep 'color0:'| awk '{print $NF}')
	RED=$(xrdb -query | grep 'color1:'| awk '{print $NF}')
	GREEN=$(xrdb -query | grep 'color2:'| awk '{print $NF}')
	YELLOW=$(xrdb -query | grep 'color3:'| awk '{print $NF}')
	BLUE=$(xrdb -query | grep 'color4:'| awk '{print $NF}')
	MAGENTA=$(xrdb -query | grep 'color5:'| awk '{print $NF}')
	CYAN=$(xrdb -query | grep 'color6:'| awk '{print $NF}')
	WHITE=$(xrdb -query | grep 'color7:'| awk '{print $NF}')
}
xrdb ~/.Xresources
getcolors
#to automatically get the width of panel would be: 
#echo "$(xrandr | grep current | awk '{print $8}') - 2*$(grep window_gap .config/bspwm/bspwmrc | awk '{print $4}')" | bc -lq

GAP=$(grep window_gap .config/bspwm/bspwmrc | awk '{print $4}')
WIDTH=$(xrandr | grep current | awk '{print $8}')
PANEL_WIDTH=$(echo "$WIDTH - 2*$GAP" | bc -lq)

pgrep -xo lemonbar > /dev/null && pkill -xo lemonbar 
pgrep -xo panel.sh > /dev/null && pkill -xo panel.sh 
panel.sh | lemonbar -p -g $(echo "$PANEL_WIDTH")x22+$GAP+5 -B "$BLACK" -F "$WHITE" -U "$CYAN" -f "xos4 terminus" -f "Font Awesome" 
xdo above -t $(xdo id -n root) $(xdo id -n lemonbar) 
