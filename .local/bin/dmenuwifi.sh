#!/bin/sh

choices=$(nmcli device wifi list | dmenu -fn mono:size=12)

network=$(echo $choices | awk '{print $1}')

echo 'Please type the password:'

read pass

sudo nmcli device wifi connect $network password $pass
