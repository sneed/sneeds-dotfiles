#!/bin/sh

chosen=$(echo "Shutdown\nRestart\nLogout\nCancel" | dmenu -i)

case "$chosen" in
    Shutdown) poweroff;;
    Restart) reboot;;
    Logout) exec i3-msg exit;;
    Cancel) exit;;
esac
						   
