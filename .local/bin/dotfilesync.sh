#!/bin/sh

cp -ru ~/.config/sway/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/sxiv ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/i3blocks/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/mpd/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/mpv/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/vifm/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/i3/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/bspwm ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/dunst/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/sxhkd/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/polybar/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/ranger/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.config/pcmanfm/ ~/.repos/sneeds-dotfiles/.config/
cp -ru ~/.emacs.d/img/ ~/.repos/sneeds-dotfiles/.emacs.d/
cp -u ~/.emacs.d/init.el ~/.repos/sneeds-dotfiles/.emacs.d/
cp -u ~/.emacs.d/config.org ~/.repos/sneeds-dotfiles/.emacs.d/
sed -i -e 's/santiago485@gmail/example@example/' -e 's/spz@firemail/example@example/' -e 's/sposada@unal/example@example/' -e 's/Santiago Posada/name/' -e 's/Zapata//' ~/.repos/sneeds-dotfiles/.emacs.d/config.org
cp -ru ~/.config/ncmpcpp/config ~/.repos/sneeds-dotfiles/.config/ncmpcpp/
cp -ru ~/.config/ncmpcpp/bindings ~/.repos/sneeds-dotfiles/.config/ncmpcpp/
cp -ru ~/.newsboat/config ~/.repos/sneeds-dotfiles/.newsboat
cp -ru ~/.newsboat/urls ~/.repos/sneeds-dotfiles/.newsboat
cp -u ~/.profile ~/.repos/sneeds-dotfiles/
cp -u ~/.Xresources ~/.repos/sneeds-dotfiles/
cp -u ~/.Xdefaults ~/.repos/sneeds-dotfiles/
cp -u ~/.bashrc ~/.repos/sneeds-dotfiles/
cp -u ~/.inputrc ~/.repos/sneeds-dotfiles/
cp -ru ~/.local/bin/ ~/.repos/sneeds-dotfiles/.local/
cp -u ~/.xinitrc  ~/.repos/sneeds-dotfiles/
cp -u ~/.repos/dwm/config.h ~/.repos/sneeds-dotfiles/.config/
cp -u ~/.repos/dwmblocks/blocks.h  ~/.repos/sneeds-dotfiles/.config/
