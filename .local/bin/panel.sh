#!/bin/sh

groups() {
    cur=`xprop -root _NET_CURRENT_DESKTOP | awk '{print $3}'`
    tot=`xprop -root _NET_NUMBER_OF_DESKTOPS | awk '{print $3}'`

    for w in `seq 0 $((cur - 1))`; do line="${line}░"; done
    line="${line}▒"
    for w in `seq $((cur + 2)) $tot`; do line="${line}░"; done
    echo $line
}

while true; do
    echo " %{l} $(groups)  $(mpc | sed 1q | cut -c 1-40)...$(mpc | sed -n 2p | awk '{print $3}') %{r}$(i3volume) $(i3brightness) $(batterystat2) Temp:$(thermals2) $(wifistatus2) $(date '+%a,%b %d,%Y %l:%M %P ')" 
    sleep 1
done

