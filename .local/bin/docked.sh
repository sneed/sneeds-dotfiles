#!/bin/sh

CAP=$(cat /sys/class/power_supply/BAT0/capacity)
STATE=$(cat /sys/class/power_supply/BAT0/status)

NUM=$(echo "($CAP-5)/1" | bc)

if [ $STATE = Charging ]; then
    sudo tlp setcharge $NUM 100
else
    exit
fi
