//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"","i3mpd",					0,		11},
	
//	{"🧠", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	30,		0},

	{"","i3volume",					0,		1},

	{"☀", "xbacklight | sed 's/\\..*//'",		0,		11},
	
	{"", "batterystat2",				5,		},

	{"🌡", "sensors | awk '/^temp1:/{print $2}'",	5,		},
	{"", "wifistatus2", 				5,		},

	{"", "date '+%a, %b %d,%Y - %l:%M %P'",		5,		},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
